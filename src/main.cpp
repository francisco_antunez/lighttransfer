
#include <opencv2/opencv.hpp>

#include <iostream>

using namespace cv;

int main() {
    Mat img;
    VideoCapture cap;

    cap.open(0);
    //cap.open(device, api);

    if(!cap.isOpened()) {
        std::cout << "Could not read open camera " << std::endl;
        return 1;
    }

    while (1) {
        cap.read(img);
        imshow("Display window", img);

        if (waitKey(0)) { // Wait for a keystroke in the window
            break;
        }
    }
    return 0;
}